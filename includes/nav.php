<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
  <div class="container-fluid">
    <a class="navbar-brand" href="./index_logged.php">Ma To-do List</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse " id="navbarNav">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link" aria-current="page" href="./my_lists.php">Mes listes</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="./create_list.php">Créer une liste</a>
        </li>
      </ul>


      <ul class="navbar-nav"> 

      <?php if ($_SESSION['auth']) { 
        echo "<form action='index.php' method='get'><li><button type='submit' name='deconnexion' class='btn btn-danger' >Deconnexion</button></li></form>";
        
       } else { 
        echo "<li class='nav-item'>
          <a class='nav-link' href='./login.php'>Connexion</a>
        </li>";
        echo "<li class='nav-item'>
          <a class='nav-link' href='./registration.php'>Inscription</a>
        </li>";
        };
        ?>
      </ul>

    </div>
  </div>
</nav>


