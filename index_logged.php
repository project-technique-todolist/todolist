<?php 
 session_start();
 $title = "Accueil";
 
//autochargement des classes:
require_once('config/autoload.php');

// include("includes/session.php");
include("./includes/header.php");
?>
    <body>
        <?php 
        // NAV
        include("./includes/nav.php");
        
        ?>

        <main class="container">
            <h1>Bienvenue sur notre site</h1>
        </main>

        <?php 
        include("./includes/footer.php");
        ?>
    </body>
</html>