<?php 
 session_start();
 $title = "Inscription";
 
//autochargement des classes:
require_once('config/autoload.php');

$userDao = new UserDao();

//récupération du tableau utilisateur (vérifier email):
$tab = $userDao->getAll();

// Déclaration des variables d'erreurs (à décommenter)
$invalidEmail = false;
$usedEmail = false;
$invalidPwd = false;
$emptyInput = false;

//si on envoie formulaire
if (isset($_POST['submit'])) {

    //si les champs sont bien remplis
    if (!empty($_POST["email"]) && !empty($_POST["password"]) && !empty($_POST["passwordConfirm"]) && !empty($_POST["name"])) { 
        $mailValid = true; 
        
        // si email n'est pas valide
        if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
            $mailValid = false;
            $invalidEmail = "Email invalide";
        }
        elseif (!$userDao->verifEmail($_POST["email"])) {
            $mailValid = false;
            $usedEmail = "Cet email existe déjà chez nous";
        }
 

        //si l'email est valide
        if ($mailValid) {
            if ($_POST["password"] == $_POST["passwordConfirm"]) {
                $hashMdp = password_hash($_POST["password"], PASSWORD_DEFAULT);
                $tab = [
                    "email" => $_POST["email"], 
                    "password" => $hashMdp,
                    "name" => htmlspecialchars($_POST["name"]) 
                ];

                $user = new User($tab);
                $userDao->add($user);
                header("Location: ./login.php");
            } else {
                $invalidPwd = "Les mots de passe ne correspondent pas";
            }           
        }
                
    } else {
        $emptyInput = "Veuillez remplir tous les champs";
    }
}

// HEADER:
 include("./includes/header.php");
?>
    <body>
        <?php 
        // NAV:
        include("./includes/nav_main.php");
        ?>

        <main class="container d-flex justify-content-center">

        <form action="#" method="post" class="w-75">
            <fieldset>
                <h1>Inscription</h1>

                <!-- Alerte Tous les champs à remplir -->
                <?php 
                if (!empty($emptyInput) && isset($emptyInput)){ ?>
                    <div class="alert alert-dismissible alert-warning">
                        <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
                        <p class="mb-0"><?=$emptyInput;?></p>
                    </div>
                <?php } ?>

                <div class="form-group">
                    <label for="name" class="form-label mt-4">Votre nom</label>
                    <input type="text" class="form-control" id="name" name="name">
                </div>

                <div class="form-group">
                    <label for="email" class="form-label mt-4">Votre email </label>
                    <input type="email" class="form-control" id="email" aria-describedby="emailHelp" name="email">
                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
                
                <!-- Alerte Email déjà utilisé -->
                <?php 
                if (!empty($usedEmail) && isset($usedEmail)){ ?>
                    <div class="alert alert-dismissible alert-warning">
                        <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
                        <p class="mb-0"><?=$usedEmail;?></p>
                    </div>
                <?php } ?>

                <!-- Alerte mail incorrect/invalide -->
                <?php 
                if (!empty($invalidEmail) && isset($invalidEmail)){ ?>
                    <div class="alert alert-dismissible alert-warning">
                        <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
                        <p class="mb-0"><?=$invalidEmail;?></p>
                    </div>
                <?php } ?>

                <div class="form-group">
                    <label for="password" class="form-label mt-4">Votre mot de passe</label>
                    <input type="password" class="form-control" id="password" name="password">
                </div>

                <div class="form-group">
                    <label for="passwordConfirm" class="form-label mt-4">Confirmer votre mot de passe</label>
                    <input type="password" class="form-control" id="passwordConfirm" name="passwordConfirm">
                </div>
                 
                <!-- Alerte Mots de passe différents -->
                <?php 
                if (!empty($invalidPwd) && isset($invalidPwd)){ ?>
                    <div class="alert alert-dismissible alert-warning">
                        <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
                        <p class="mb-0"><?=$invalidPwd;?></p>
                    </div>
                <?php } ?>

                <button type="submit" name="submit" class="btn btn-primary my-3">S'inscrire</button>
            </fieldset>
        </form>

        </main>
            
        <?php 
        // FOOTER:
        include("./includes/footer.php");
        ?>
    </body>
</html>