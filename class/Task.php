<?php 
class Task implements JsonSerializable
{
    private $id_task;
    private $id_lists;
    private $content;

    /*
     * Get the value of id_lists
     */ 
    public function getId_lists()
    {
        return $this->id_list;
    }

    /*
     * Set the value of id_lists
     *
     * @return  self
     */ 
    public function setId_lists($id_lists)
    {
        $this->id_lists = $id_lists;

        return $this;
    }

    /*
     * Get the value of content
     */ 
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set the value of content
     *
     * @return  self
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /*
     * Get the value of id
     */ 
    public function getId_task()
    {
        return $this->id_task;
    }

    /**
     * Set the value of id_task
     *
     * @return  self
     */
    public function setId_task($id_task)
    {
        $this->id_task = $id_task;

        return $this;
    }

    public function jsonSerialize() {
        return (object) get_object_vars($this);
    }

}

?>
