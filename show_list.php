<?php
session_start();
require_once('config/autoload.php');
$listDao = new ListDao();
$allListes = $listDao->getAll();
var_dump($allListes);
$taskDao = new TaskDao();
$allTask = $taskDao->getAll();
var_dump($allTask);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Affichage liste</title>
</head>

<body>
    <?php
    include('includes/nav.php');
    ?>

    <h1>Mes listes</h1>
    <?php
    foreach ($allListes as $value) {
        $id = (int)$value->getId_lists();
    ?>
        <div class="card border-warning mb-3" style="max-width: 18rem;">
            <div class="card-header"><?= $value->getTitle()  ?></div>

            <div class="card-body">
                <?= var_dump($id); ?>
                <h5 class="card-title"><?= $taskDao->getAllById($id)->getContent() ?></h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            </div>
        </div>


    <?php } ?>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>

</html>