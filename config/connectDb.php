<?php 
require_once('config/conf.php');

function connectDB(){
    try {
        $bdd = new PDO('mysql:host='.DB_HOST.';dbname='.DB_BASE.';charset=utf8', DB_USER, DB_PWD,
        array(PDO::ATTR_ERRMODE =>
        PDO::ERRMODE_EXCEPTION));
        return $bdd;
    } catch (PDOException $err) {
        $msg = 'ERREUR PDO dans ' . $err->getFile() .
        ' L.' . $err->getLine() . ' : ' .
        $err->getMessage();
        die($msg);
    }
}
?>