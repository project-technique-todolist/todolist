<?php
require_once('class/ModelDao.php');
class TaskDao extends ModelDao
{
    public function addModal($task, $id)
    {
        $q = $this->_db->prepare('INSERT INTO task (content, id_lists) 
        VALUES (:content, :id_lists)');
        $q->bindValue(':content', $task, PDO::PARAM_STR);
        $q->bindValue(':id_lists', $id, PDO::PARAM_INT);

        $q->execute();
    }

    public function addTask($task, $id)
    {
        $q = $this->_db->prepare('INSERT INTO task (content, id_lists) 
        VALUES (:content, :id_lists)');
        $q->bindValue(':content', $task['content'], PDO::PARAM_STR);
        $q->bindValue(':id_lists', $id, PDO::PARAM_INT);

        $q->execute();
    }


    public function update($task, $id, $id_task)
    {
        $q = $this->_db->prepare('UPDATE task SET content = :content
        WHERE id_lists = :id and id_task = :id_task');
        $q->bindValue(':content', $task, PDO::PARAM_STR);
        $q->bindValue(':id', $id, PDO::PARAM_INT);
        $q->bindValue(':id_task', $id_task, PDO::PARAM_INT);


        $q->execute();
    }

    public function delete(INT $id)
    {
        $this->_db->query('DELETE FROM task WHERE id_task =' . $id);
    }

    public function get($id)
    {
        $id = (int) $id;
        $q = $this->_db->query('SELECT FROM task WHERE id_lists =' . $id);

        return $q->fetchObject(Task::class);
    }

    public function getAll()
    {
        $q = $this->_db->query('SELECT * FROM task');
        return $q->fetchAll(PDO::FETCH_CLASS, Task::class);
    }

    public function getAllById($id)
    {
        $q = $this->_db->query("SELECT * FROM task WHERE id_lists = $id");
        // return $q->fetchAll(PDO::FETCH_OBJ);
        // return $q->fetchObject(Task::class);
        return $q->fetchAll(PDO::FETCH_CLASS, Task::class);
        // return $q->setFetchMode(PDO::FETCH_CLASS, 'Task');

    }
}
