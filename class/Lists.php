<?php
class Lists
{
    private $id_lists;
    private $title;
    private $date;
    private $id_user;
    private $tasks;

    public function __construct() {
        $taskDao = new TaskDao();
        $this->setTasks($taskDao->getAllById($this->getId_lists()));
    }
    
    /*
     * Get the value of id_list
     */ 
    public function getId_lists()
    {
        return $this->id_lists;
    }

    /*
     * Set the value of id_list
     *
     * @return  self
     */ 
    public function setId_lists($id_lists)
    {
        $this->id_lists = $id_lists;

        return $this;
    }

    /*
     * Get the value of title
     */ 
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of title
     *
     * @return  self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of date
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set the value of date
     *
     * @return  self
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get the value of id_user
     */
    public function getId_user()
    {
        return $this->id_user;
    }

    /**
     * Set the value of id_user
     *
     * @return  self
     */
    public function setId_user($id_user)
    {
        $this->id_user = $id_user;

        return $this;
    }

    /**
     * Get the value of tasks
     */ 
    public function getTasks()
    {
        return $this->tasks;
    }

    /**
     * Set the value of tasks
     *
     * @return  self
     */
    public function setTasks($tasks)
    {
        $this->tasks = $tasks;

        return $this;
    }
}
