<?php 
session_start();
$title = "Connexion";

//autochargement des classes:
require_once('config/autoload.php');

$userDao = new UserDao();

$users = $userDao->getAll();

$champsError = false;
$identError = false;
$passError = false;


if (isset($_POST['submit'])) {
    if (!empty($_POST['email']) && !empty($_POST['password'])) {
        if ($userDao->getUserByEmail($_POST['email'])) {
            if ($userDao->verifMdp($_POST['email'], $_POST['password'])){
                $user = new User($userDao->getUserByEmail($_POST['email']));
                $_SESSION['user'] = serialize($user);
                $_SESSION["auth"] = true;
                header("Location: ./my_lists.php");
            }else{
                $passError = "Mot de passe incorrect";
            }
        }else {
            $identError = "Cet utilisateur n'existe pas!";
        }

    }else{
        $champsError = "Vous devez remplir les champs!";
    }
}

include("./includes/header.php");
?>

    <body>
        <?php 
        // NAV
        include("./includes/nav_main.php");
        ?>

        <main class="container d-flex justify-content-center">

            <form action="#" method="post" class="w-75">
            <fieldset>
                <h1>Connexion</h1>

                <!-- Alerte Tous les champs à remplir -->
                <?php 
                if (!empty($champsError) && isset($champsError)){ ?>
                    <div class="alert alert-dismissible alert-warning">
                        <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
                        <p class="mb-0"><?=$champsError;?></p>
                    </div>
                <?php } ?>

                <div class="form-group">
                    <label for="email" class="form-label mt-4">Votre email </label>
                    <input type="email" class="form-control" id="email" aria-describedby="emailHelp" name="email">
                </div>

                <!-- Alerte mail incorrect/invalide -->
                <?php 
                if (!empty($identError) && isset($identError)){ ?>
                    <div class="alert alert-dismissible alert-warning">
                        <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
                        <p class="mb-0"><?=$identError;?></p>
                    </div>
                <?php } ?>

                <div class="form-group">
                    <label for="password" class="form-label mt-4">Votre mot de passe</label>
                    <input type="password" class="form-control" id="password" name="password">
                </div>

                <!-- Mot de passe incorrect -->
                <?php 
                if (!empty($passError) && isset($passError)){ ?>
                    <div class="alert alert-dismissible alert-warning">
                        <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
                        <p class="mb-0"><?=$passError;?></p>
                    </div>
                <?php } ?>

                <button type="submit" name="submit" class="btn btn-primary my-3">Se connecter</button>
            </fieldset>
            </form>
        </main>

        <?php 
        include("./includes/footer.php");
        ?>
    </body>
