// Traitement du modal de suppression
let suppModal = document.getElementById('suppModal');
suppModal.addEventListener('show.bs.modal', function (event) {
    // Button that triggered the modal
    var button = event.relatedTarget;
    // Extract info from data-bs-* attributes
    var id = button.getAttribute('data-bs-id');
    // Update the modal's content.
    var IdInput = suppModal.querySelector('#suppInput');
    IdInput.value = id;
})

