<?php 
require_once('class/ModelDao.php');
class ListDao extends ModelDao
{

    public function add($list, $id){
        $q = $this->_db->prepare('INSERT INTO lists (title, date, id_user) 
        VALUES (:title, NOW(), :id_user)');
        $q->bindValue(':title',$list['title'] ,PDO::PARAM_STR);
        $q->bindValue(':id_user',$id ,PDO::PARAM_STR);
        $q->execute();
        return $this->_db->lastInsertId();
    }

    public function update(Lists $list) {
        $q = $this->_db->prepare('UPDATE lists SET title = :title WHERE id_user = :id');
        $q->bindValue(':title', $list->getTitle(), PDO::PARAM_STR);

        $q->execute();
    }

    public function delete(INT $id) {
        $query = "DELETE FROM lists WHERE id_lists = ?";
        $stmt = $this->_db->prepare($query);
        $stmt->bindValue(1, $id, PDO::PARAM_STR);
        $stmt->execute();
    }

    public function get($id) {
        
        $q = $this->_db->query('SELECT * FROM lists WHERE id_user ='.$id);

        //return $q->fetchObject(User::class);
        return $q->fetchAll(PDO::FETCH_CLASS, Lists::class);

    }

    public function getAll() {
        $q = $this->_db->query('SELECT * FROM lists');
        return $q->fetchAll(PDO::FETCH_CLASS, Lists::class);
    }
    
}
?>