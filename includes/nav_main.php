<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
  <div class="container-fluid">
    <a class="navbar-brand" href="./index.php">Ma To-do List</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse " id="navbarNav">
        <span class="empty me-auto mb-2 mb-lg-0"></span>
      <ul class="navbar-nav"> 
        <li class='nav-item'>
          <a class='nav-link' href='./login.php'>Connexion</a>
        </li>
        <li class='nav-item'>
          <a class='nav-link' href='./registration.php'>Inscription</a>
        </li>
    </ul> 
    </div>
  </div>
</nav>
