<?php 
 session_start();
 $title = "Mes listes";

//autochargement des classes:
require_once('config/autoload.php');

// Recupération de l'utilisateur connecté
$user = unserialize($_SESSION["user"]);

$listDao = new ListDao();
$taskDao = new TaskDao();


// Traitement de la suppression d'une liste après confirmation via un modal
if (isset($_GET["supp"])) {
    // Suppression
    $listDao->delete($_GET["supp"]);
    
    $_SESSION["user"] = serialize($user);
    header("Location: my_lists.php");
}

// Traitement de la modification de taches via modal
if (isset($_POST["modif"])) {
    for ($i=0; $i < (count($_POST)/2) -2; $i++) {
        $taskDao->update($_POST['task'.$i], $_POST['id_list'],$_POST['id_task'.$i]); 
    }
}

if (isset($_POST["btnSup"])) {
   $taskDao->delete($_POST["btnSup"]);
}

if (isset($_POST["addTask"])) {
    $taskDao->addModal($_POST["inputTask"], $_POST["addTask"]);
 }

$lists = $listDao->get($user->getId_user());

include("includes/session.php");
include("./includes/header.php");
?>
    <body>
        <?php 
        // NAV
        include("./includes/nav.php");
        ?>

        <main class="container">

        <h1>Mes listes</h1>

        <div class="d-flex flex-wrap">
        
        <?php
    foreach ($lists as $value) { ?>
        <div data-bs-toggle="modal" data-bs-target="#exampleModal" data-bs-target="#exampleModal" data-bs-title="<?=$value->getTitle()?>" data-bs-id="<?=$value->getId_lists()?>" data-bs-task='<?=json_encode($value->getTasks())?>'class="card border-warning m-3" style="width: 18rem;">
            <div class="card-header"><?= $value->getTitle()  ?></div>

            <div class="card-body">
                <ul>
                <?php
                if ($value->getTasks()) { 
                    foreach ($value->getTasks() as $valueTask) {
                    ?>
                        <li><?= $valueTask->getContent()  ?></li>
                    <?php
                    }
                } ?>
                </ul>
                <button type="submit" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#suppModal" data-bs-id="<?=$value->getId_lists()?>" > Supprimer</button>
            </div>
        </div>

    <?php } ?>

    <!-- SHOW MODAL  -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="#" method="post">
                    <div class="modal-body">   
                        <div class="mb-3" id="tacheInput">
                            
                        </div> 
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
                        <button type="submit" name="modif" class="btn btn-primary">Modifier</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- MODAL SUPPRESSION : OK!-->
    <div class="modal fade" id="suppModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Confirmation de suppression</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>Etes vous sur de vouloir supprimer cette liste ?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
                    <form action="#" method="get">
                        <input type="hidden" name="supp" id="suppInput">
                        <input type="hidden" name="image" id="image">
                        <button type="submit" id="suppBtn" name="suppBtn" class="btn btn-danger">Confirmer</button>
                    </form>
                </div>
                </div>
            </div>
        </div>
        
        </main>
        
        <?php 

        include("./includes/footer.php");
        ?>

        <script src="js/modalTask.js"></script>
        <script src="./js/modal.js"></script>
    </body>

<body>
