<?php
class User
{

    private $id_user;
    private $email;
    private $name;
    private $password;
    private $lists;

    public function __construct($valeurs = array())
    {
        foreach ($valeurs as $key => $value) {
            $method = "set" . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
        
    }

    //public function __construct() {
        //$listDao = new ListDao();
        //$this->setLists($listDao->get($this->getId_user()));
    //}

    /**
     * Get the value of id_user
     */
    public function getId_user()
    {
        return $this->id_user;
    }

    /**
     * Set the value of id_user
     *
     * @return  self
     */
    public function setId_user($id_user)
    {
        $this->id_user = $id_user;

        return $this;
    }

    /**
     * Get the value of email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set the value of password
     *
     * @return  self
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get the value of lists
     */ 
    public function getLists()
    {
        return $this->lists;
    }

    /**
     * Set the value of lists
     *
     * @return  self
     */ 
    public function setLists($lists)
    {
        $this->lists = $lists;

        return $this;
    }
}
