var exampleModal = document.getElementById('exampleModal')
exampleModal.addEventListener('show.bs.modal', function (event) {
    // Button that triggered the modal
    var button = event.relatedTarget
    // Extract info from data-bs-* attributes
    var recipient = button.getAttribute('data-bs-id')
    var title = button.getAttribute('data-bs-title')
    var tab = button.getAttribute('data-bs-task')

    tache = JSON.parse(tab)
    var tacheInput = document.getElementById("tacheInput")
    tacheInput.innerHTML = `<label for="tache" class="col-form-label d-flex">Tache: </label>
                            <input id="id_list" name="id_list" type="hidden" value="${recipient}">`;

    


    for (let index = 0; index < tache.length; index++) {
        const element = tache[index];
        let modalTache = document.createElement("input");
        let modalTache1 = document.createElement("input");
        let btnSup = document.createElement("button");
        
        
        modalTache.value = element['content'];
        modalTache.setAttribute("name", "task"+index);
        
        modalTache1.value = element['id_task'];
        modalTache1.setAttribute("name", "id_task"+index);
        modalTache1.setAttribute("type", "hidden");

        btnSup.setAttribute('name', 'btnSup');
        btnSup.setAttribute("type", "submit");
        btnSup.setAttribute("class", "btn btn-danger");
        btnSup.textContent = 'X';
        btnSup.value = element['id_task'];


        tacheInput.appendChild(modalTache);
        tacheInput.appendChild(modalTache1);
        tacheInput.appendChild(btnSup);

    }

    let addTask = document.createElement("button");
    let inputTask = document.createElement("input")



    addTask.setAttribute('name', 'addTask');
    addTask.setAttribute("class", "btn btn-primary");
    addTask.setAttribute('type', 'submit');
    addTask.textContent = '+';
    addTask.value = recipient;

    inputTask.setAttribute('name', 'inputTask');
    inputTask.setAttribute('placeholder', 'Ajouter une nouvelle tache');

    
    tacheInput.appendChild(inputTask);
    tacheInput.appendChild(addTask);
    

    
    var modalTitle = exampleModal.querySelector('.modal-title')
    
    modalTitle.textContent = title
})