<?php 
 session_start();
 $title = "Création d'une liste";
 
//autochargement des classes:
require_once('config/autoload.php');

// Recupération de l'utilisateur connecté
$user = unserialize($_SESSION["user"]);

$listDao = new ListDao();
$taskDao = new TaskDao();

$emptyInput = false;
// Si on envoie le formulaire
if (isset($_POST['submit'])) {
    // Si contenu (content) est rempli
    if (!empty($_POST['title'] && empty($_POST['content'])) ) {

        $id = $listDao->add($_POST,$user->getId_user());
        echo "liste enregistrée";
        header("Location: my_lists.php");
    }
    if (!empty($_POST['title'] && !empty($_POST['content'])) ) {

        $id = $listDao->add($_POST,$user->getId_user());
        $taskDao->addTask($_POST, $id);
        echo "liste enregistrée";
        header("Location: my_lists.php");
    }
    if (!empty($_POST['content']) && empty($_POST['title'])) {
            $_POST['title'] = "Sans titre";
            $id = $listDao->add($_POST,$user->getId_user());
            $taskDao->addTask($_POST, $id);

            echo "liste enregistrée";
            header("Location: my_lists.php");
    } else {
        $emptyInput = "Veuillez remplir tous les champs";
    }

}

include("includes/session.php");
include("includes/header.php");
?>
    <body>
        <?php 
        // NAV
        include("includes/nav.php");
        ?>

        <main class="container d-flex justify-content-center">

        <form action="#" method="post">

        <h1>Création d'une liste</h1>
         
            <div class="form-group">
                <label for="title" class="form-label mt-4">Intitulé : </label>
                <input type="text" class="form-control" id="title" aria-describedby="emailHelp" name="title">
            </div>

            <div class="form-group">
                <label for="content" class="form-label mt-4">Tâches : </label>
                <input type="text" class="form-control" id="content" aria-describedby="emailHelp" name="content">
            </div>

            <button type="submit" name="submit" class="btn btn-primary my-3">Enregistrer</button>
            <!-- Alerte Tous les champs à remplir -->
            <?php 
            if ($emptyInput) {
                echo $emptyInput;
            }
            ?>
        </form>
        
        </main>
        <?php 
        include("./includes/footer.php");
        ?>
    </body>
</html>