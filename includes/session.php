<?php 
if (!isset($_SESSION["auth"])) {
    $_SESSION["auth"] = false;
}

if (isset($_GET["deconnexion"])) { 
    $_SESSION["auth"] = false; 
} 

if ($_SESSION["auth"] == false) {
    header("Location: ./index.php");
}
?>